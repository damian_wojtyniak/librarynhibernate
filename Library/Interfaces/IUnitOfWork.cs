﻿using System;

namespace Library.Interfces
{
    interface IUnitOfWork : IDisposable
    {
        IAuthorRepository Authors { get; }
        IBookRepository Books { get; }
        IReaderRepository Readers { get; }

        void Save();
    }
}
