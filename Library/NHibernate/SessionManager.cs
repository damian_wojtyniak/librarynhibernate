﻿using NHibernate;
using NHibernate.Context;

namespace Library.NHibernate
{
    public class SessionManager : ICurrentSessionContext
    {
        public static readonly SessionManager Instance = new SessionManager();

        protected static ISessionFactory sessionFactory;

        private SessionManager()
        {
            sessionFactory = DatabaseConfiguration.GetSessionFactory();
        }

        public ISession CurrentSession()
        {
            if (CurrentSessionContext.HasBind(sessionFactory))
            {
                return sessionFactory.GetCurrentSession();
            }

            var session = sessionFactory.OpenSession();
            CurrentSessionContext.Bind(session);

            return session;
        }

        public void Unbind()
        {
            CurrentSessionContext.Unbind(sessionFactory);
        }
    }
}