﻿using Library.Interfces;
using System;

namespace Library.NHibernate
{
    class UnitOfWork : IUnitOfWork, IDisposable
    {
        public IAuthorRepository Authors { get; }
        public IBookRepository Books { get; }
        public IReaderRepository Readers { get; }

        private readonly SessionManager _sessionFactory;

        public UnitOfWork(SessionManager sessionFactory)
        {
            _sessionFactory = sessionFactory;

            Authors = new AuthorRepository(sessionFactory);
            Books = new BookRepository(sessionFactory);
            Readers = new ReaderRepository(sessionFactory);
        }

        public void Save()
        {
            _sessionFactory.CurrentSession().Close();
        }

        public void Dispose()
        {
            _sessionFactory.CurrentSession().Dispose();
            _sessionFactory.Unbind();
        }
    }
}
