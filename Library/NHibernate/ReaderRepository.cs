﻿using Library.Interfces;
using Library.Models;

namespace Library.NHibernate
{
    class ReaderRepository : Repository<Reader>, IReaderRepository
    {
        public ReaderRepository(SessionManager sessionFactory) : base(sessionFactory) {}
    }
}
