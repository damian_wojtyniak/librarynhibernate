﻿using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Data.SqlClient;
using System.Reflection;

namespace Library.NHibernate
{
    class DatabaseConfiguration
    {
        const string DIALECT = "NHibernate.Dialect.MsSql2012Dialect";
        const string CONNECTION_PROVIDER = "NHibernate.Connection.DriverConnectionProvider";
        const string CONNECTION_STRING_SERVER = @"Server=(localdb)\MSSQLLocalDB; Integrated Security=True; Connection Timeout=10;";
        const string DATABASE_NAME = "NHibernateDb";
        const string CONNECTION_STRING_DB = @"initial catalog=NHibernateDb;";
        const string CONNECTION_STRING = CONNECTION_STRING_SERVER + CONNECTION_STRING_DB;

        readonly static string[] HBM_ENTITY_FILES =
        {
            "Library.Mappings.Author.hbm.xml",
            "Library.Mappings.Book.hbm.xml",
            "Library.Mappings.Reader.hbm.xml"
        };

        private static ISessionFactory sessionFactory;
        private static Configuration nHibernateConfig;

        public static ISessionFactory GetSessionFactory()
        {
            if (sessionFactory == null || sessionFactory.IsClosed)
            {
                nHibernateConfig = new Configuration();

                nHibernateConfig.SetProperty("dialect", DIALECT);
                nHibernateConfig.SetProperty("connection.provider", CONNECTION_PROVIDER);
                nHibernateConfig.SetProperty("connection.connection_string", CONNECTION_STRING);
                nHibernateConfig.SetProperty("current_session_context_class", "thread_static");

                foreach (string resource in HBM_ENTITY_FILES)
                    nHibernateConfig.AddResource(resource, Assembly.GetExecutingAssembly());

                ExportSchema();

                sessionFactory = nHibernateConfig.BuildSessionFactory();
            }
            return sessionFactory;
        }

        private static void CreateDatabase()
        {
            var connection = new SqlConnection();
            connection.ConnectionString = CONNECTION_STRING_SERVER;
            
            try
            {
                var command = new SqlCommand();
                
                command.Connection = connection;
                command.CommandText = "IF NOT EXISTS " +
                                    "(SELECT name FROM master.dbo.sysdatabases " +
                                    $"WHERE name = '{DATABASE_NAME}') " +
                                    $"CREATE DATABASE {DATABASE_NAME};";
                
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw new Exception("Cannot create database");
            }
            finally
            {
                connection.Close();
            }
        }

        public static void ExportSchema()
        {
            var schemaExport = new SchemaExport(nHibernateConfig);
           
            if (schemaExport == null)
                CreateDatabase();

            //schemaExport.Execute(false, true, false);
        }
    }
}
