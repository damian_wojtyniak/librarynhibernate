﻿using System.Collections.Generic;

namespace Library.Models
{
    public class Book
    {
        public virtual int Year { get; set; }
        public virtual int Id { get; set; }
        public virtual string Title { get; set; }
        public virtual string Publisher { get; set; }

        public virtual int? ReaderId { get; set; }
        public virtual Reader ActualReader { get; set; }
        public virtual ICollection<Author> Authors { get; set; }

        public override string ToString()
        {
            return $"{Id}. {Title}, {Publisher}, {Year}";
        }
    }
}