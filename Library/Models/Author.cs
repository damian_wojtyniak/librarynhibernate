﻿using System.Collections.Generic;

namespace Library.Models
{
    public class Author
    {
        public virtual int Id { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }

        public virtual ICollection<Book> Books { get; set; }

        public override string ToString()
        {
            return $"{Id}. {FirstName}, {LastName}";
        }
    }
}

