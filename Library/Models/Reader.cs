﻿using System.Collections.Generic;

namespace Library.Models
{
    public class Reader
    {
        public virtual int Id { get; set; }
        public virtual int Number { get; set; }
        public virtual ICollection<Book> Loans { get; set; }

        public override string ToString()
        {
            return $"{Id}. {Number}";
        }
    }
}
