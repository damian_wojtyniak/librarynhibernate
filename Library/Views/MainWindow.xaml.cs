﻿using Library.Interfces;
using Library.Models;
using Library.NHibernate;
using System.Windows;
using System.Windows.Input;

namespace RemoteCourses.Views
{
    public partial class MainWindow : Window
    {
        private IUnitOfWork unitOfWork;

        public MainWindow()
        {
            InitializeComponent();

            unitOfWork = new UnitOfWork(SessionManager.Instance);

            DataContext = unitOfWork;
        }

        private void RefreshReadersMethod()
        {
            var readers = unitOfWork.Readers.GetAll();

            readersListBox.ItemsSource = null;
            readersListBox.ItemsSource = readers;
        }

        private void RefreshAuthorsMethod()
        {
            var authors = unitOfWork.Authors.GetAll();

            authorsListBox.ItemsSource = null;
            authorsListBox.ItemsSource = authors;
        }

        private void RefreshBooksMethod()
        {
            var books = unitOfWork.Books.GetAll();
            var readers = unitOfWork.Readers.GetAll();

            booksListBox.ItemsSource = null;
            booksListBox.ItemsSource = books;

            borrowReadersListBox.ItemsSource = null;
            borrowReadersListBox.ItemsSource = readers;
        }

        private void RefreshReaders(object sender, RoutedEventArgs e)
        {
            RefreshReadersMethod();
        }

        private void AddReader(object sender, RoutedEventArgs e)
        {
            bool validation = int.TryParse(addReaderNumberTextBox.Text, out int value);
            if (!validation) return;

            Reader reader = new Reader();
            reader.Number = value;
            
            unitOfWork.Readers.Add(reader);
            RefreshReadersMethod();
        }

        private void RefreshAuthors(object sender, RoutedEventArgs e)
        {
            RefreshAuthorsMethod();
        }

        private void RefresBooks(object sender, RoutedEventArgs e)
        {
            RefreshBooksMethod();
        }

        private void BorrowBook(object sender, RoutedEventArgs e)
        {
            var book = booksListBox.SelectedItem as Book;
            var reader = borrowReadersListBox.SelectedItem as Reader;

            if (reader == null || book == null)
                return;

            if (book.ActualReader != null)
            {
                MessageBox.Show("Ta książka została już wypożyczona");
                return;
            }

            book.ActualReader = reader;

            unitOfWork.Books.Update(book);

            MessageBox.Show("Wypożyczono ksiązkę");
        }

        private void AcceptReturn(object sender, RoutedEventArgs e)
        {
            var book = booksListBox.SelectedItem as Book;
            var reader = borrowReadersListBox.SelectedItem as Reader;

            if (reader == null || book == null)
                return;

            if (book.ActualReader != reader)
            {
                MessageBox.Show("Wybrany użytkownik nie jest w posiadaniu tej książki");
                return;
            }

            book.ActualReader = null;
            reader.Loans.Remove(book);

            unitOfWork.Books.Update(book);

            MessageBox.Show("Zwrócono książkę");
        }

        private void authorsBooksDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var author = authorsListBox.SelectedItem as Author;
            var books = author.Books;

            authorBooksListBox.ItemsSource = null;
            authorBooksListBox.ItemsSource = books;
        }

        private void DeleteReader(object sender, RoutedEventArgs e)
        {
            var reader = readersListBox.SelectedItem as Reader;

            unitOfWork.Readers.Remove(reader);
            RefreshReadersMethod();
        }
    }
}
